## __Application__
### __Mybinder__
Jupyter est __une application web__ utilisée pour programmer dans plus de <font color='green'>40</font> langages de programmation, dont ___Python, Julia, R, etc___.
_C'est un projet communautaire dont l'objectif est de développer des logiciles libres, des formats ouverts et des services pour l'informatique interactive._
* Source([https://fr.wikipedia.org/wiki/Jupyter])
<img src=https://fr.wikipedia.org/wiki/Jupyter#/media/Fichier:Jupyter_logo.svg />